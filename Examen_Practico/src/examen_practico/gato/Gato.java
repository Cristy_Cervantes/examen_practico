/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen_practico.gato;

/**
 *
 * @author alemu
 */
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import  javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class Gato extends JFrame implements ActionListener {
    
    int v1, v2, v3, v4, v5, v6, v7, v8, v9;
    boolean done = false;
    boolean fin = false;
    
    JButton iniciar;
    JButton tablero [][];
    String jugador1, jugador2;
    int turno= -1;
    JLabel mensaje;
    Color colorB;
    JButton Computador;
    
    
    public Gato (){
        this.setLayout(null);
        mensaje= new JLabel("GATO");
        mensaje.setBounds(150,40,200,30);
        this.add(mensaje);
        
        iniciar= new JButton ("iniciar juego dos jugadores");
        iniciar.setBounds(155,350,190,30);
        iniciar.addActionListener(this);
        this.add(iniciar);
        tablero= new JButton [3][3];
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
            tablero [i][j] = new JButton();
            tablero [i][j].setBounds((i+1)*80+40,(j+1)*80,80,90);
            this.add(tablero[i][j]);
            tablero[i][j].addActionListener(this);
        }
        }
    colorB = tablero[0][0].getBackground();
    
      Computador = new JButton ("Contra computador")  ;
      Computador.setBounds(175,400,150,30);
      Computador.addActionListener(this);
      this.add(Computador);
   
        }
         
    
    
    public static void main (String[]args){
        Gato ventana = new Gato();
        ventana.setDefaultCloseOperation(3);
        ventana.setSize(500,500);
        ventana.setLocationRelativeTo(null);
        ventana.setResizable(false);
        ventana.setTitle("Juego Gato");
        ventana.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {                               
       //vs maquina
       if (e.getSource()==Computador){
            turno=0;
            JOptionPane.showMessageDialog(this,"Preparando el Juego");
            jugador1= JOptionPane.showInputDialog(this,"Ingresa tu nombre");            
            mensaje.setText("Turno del jugador "+ jugador1);
            limpiar();
            
        }
        

        //dos jugadores
        if (e.getSource()==iniciar){
            turno=0;
            JOptionPane.showMessageDialog(this,"Iniciando el Juego");
            jugador1= JOptionPane.showInputDialog(this,"Ingresa nombre del primer jugador");
            jugador2= JOptionPane.showInputDialog(this,"Ingresa nombre del segundo jugador");
            mensaje.setText("Turno del jugador "+ jugador1);
            limpiar();
            
        } else{
            JButton boton= (JButton) e.getSource();
           if (turno==0){
            if (boton.getText().equals("")){
                boton.setBackground(Color.BLUE);
                boton.setText("X");
                boton.setEnabled(false);
                turno= 1;
                mensaje.setText("Turno del jugador "+ jugador2);
            }
            } else{
                if(turno==1){
                    if (boton.getText().equals("")){
                boton.setBackground(Color.GREEN);
                boton.setText("O");
                boton.setEnabled(false);
                turno= 0;
                mensaje.setText("Turno del jugador " +jugador1);
                }
            }
            
        }
           verificar();
    }
    
    
}
    public void verificar(){
      //horizontales x
        int ganador=0;
        for(int i=0; i<3; i++){
            if (tablero [0][i].getText().equals("X") && tablero[1][i].getText().equals("X") 
                    && tablero[2][i].getText().equals("X")){
                ganador=1;
            }               
      //verticales x     
            if (tablero [i][0].getText().equals("X") && tablero[i][1].getText().equals("X") 
                    && tablero[i][2].getText().equals("X")){
                ganador=1;
            }
            
        }
        //diagonales x
        if(tablero [0][0].getText().equals("X") && tablero[1][1].getText().equals("X") 
                    && tablero[2][2].getText().equals("X")){
                ganador=1;
            }
        
        if(tablero [0][2].getText().equals("X") && tablero[1][1].getText().equals("X") 
                    && tablero[2][0].getText().equals("X")){
                ganador=1;
            }
        
          //horizontales o   
        for(int i=0; i<3; i++){
            if (tablero [0][i].getText().equals("O") && tablero[1][i].getText().equals("O") 
                    && tablero[2][i].getText().equals("O")){
                ganador=1;
            }
      //verticales o     
            if (tablero [i][0].getText().equals("O") && tablero[i][1].getText().equals("O") 
                    && tablero[i][2].getText().equals("O")){
                ganador=1;
            }
            
        }
        //diagonales o
        if(tablero [0][0].getText().equals("O") && tablero[1][1].getText().equals("O") 
                    && tablero[2][2].getText().equals("O")){
                ganador=1;
            }
        
        if(tablero [0][2].getText().equals("O") && tablero[1][1].getText().equals("O") 
                    && tablero[2][0].getText().equals("O")){
                ganador=1;
            }
        
        
        if (ganador==1){
            JOptionPane.showMessageDialog(this, "Felicidades "+jugador1+ " ganaste!");
            bloquear();
        }
        
        
         if (ganador==2){
            JOptionPane.showMessageDialog(this, "Felicidades "+jugador2+ " ganaste!");
            bloquear();
        }
    }

            
         public void bloquear(){
        for (int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                tablero[i][j].setEnabled(false);            
            }
        }
    }    
            
    public void limpiar(){
        for (int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                tablero[i][j].setEnabled(true);
                tablero[i][j].setText("");
                tablero[i][j].setBackground(colorB);
            }
        }
    }
}
